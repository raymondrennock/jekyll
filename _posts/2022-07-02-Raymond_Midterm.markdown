---
layout: post
title:  "Professional Biography"
date:   2022-07-28 12:01:18 -0300
categories: UMKC Project
---


<hr> 

<br>

Hello, I'm Raymond Rennock. Currently, I'm employeed at Office Depot as the Technical Support Manager. It's been a rocky few years for me as I've recently came back to finish my schooling after taking a longer break in order to pay off my bills, and now I've finally reached the end of my schooling journey. I've returned and now I'm ready to prove myself to the world. I've lived a rather quiet life for the most part, but I knew from a young age that I wanted to focus on the field of Computers.

<div>
    <p style="float: right;"><img src="https://i.imgur.com/cCfG0zd.jpg" height="200px" width="200px" border="5px"></p>
</div>

After graduating from Shawnee Mission West, I enrolled in UMKC originally in the Mechanical Engineering program, though after a year I realized that this is what I wanted was to focus in the Computer Science field. As a result, I spent 2017 to 2020 in the Computer Science field while also pursuing mathematics due to my interest in the subject. Unfortunately, due to COVID-19 as well as monetary concerns, I was unable to finish my education and as a result I was forced to take a two year hiatus.

For this reason, I spent time looking for a job until ultimately I found a position at Office Depot, where over the course of a year I went from a Cashier to becoming the Head of the Print Shop and now I've become a manager overseeing the tech support team. Otherwise, I've spent these years focusing on my creative writing in hopes of one day publishing a more professional novel.

<br>

<hr>

