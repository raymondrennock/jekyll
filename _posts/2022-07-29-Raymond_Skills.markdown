---
layout: post
title:  "Skills"
date:   2022-07-29 17:45:13 -0300
categories: UMKC Project
---

<hr>

<br>

While the Resume and Courses may give a general knowledge of my skills, I've taken the liberty to list them here. I plan to spend some time to give a more detailed explanation on my proficiency in each subject later on, but for now I've simply listed them.

<br>

<hr>

<br>

<ul><b>- C++</b></ul>
<ul><b>- Java</b></ul>
<ul><b>- C#</b></ul>
<ul><b>- HTML</b></ul>
<ul><b>- CSS</b></ul>
<ul><b>- Python</b></ul>
<ul><b>- SQL</b></ul>
<ul><b>- Graphic Design</b></ul>
