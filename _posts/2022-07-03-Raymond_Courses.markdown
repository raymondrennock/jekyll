---
layout: post
title:  "Education Background"
date:   2022-07-03 10:00:00 -0300
categories: UMKC Project
---

<hr>

<br>

As a foreword, I've taken many classes over the course of my stay at UMKC, and I will be listing the ones relevant with their class number and the full title. I've become proficient in C++, Javascript, and Python among other languages.

<br>

<hr>

<br>

<ul><b>- Statistics 235</b> - Elementary Statistics</ul>
<ul><b>- Physics 240/250</b> - Physics for Scientists and Engineers I & II</ul>
<ul><b>- Math 266/268</b> - Accelerated Calculus I & II</ul>
<ul><b>- Math 300</b> - Linear Algebra I</ul>
<ul><b>- Math 301</b> - Solid Ground: Sets and Proof</ul>
<ul><b>- Computer Science 101/201</b> - Problem Solving and Programming I & II</ul>
<ul><b>- Computer Science 281R</b> - Introduction to Computer Architecture and Organization</ul>
<ul><b>- Computer Science 191/291</b> - Discrete Structures I & II</ul>
<ul><b>- Computer Science 303</b> - Data Structures</ul>
<ul><b>- Computer Science 394R</b> - Applied Probability</ul>
<ul><b>- Computer Science 404</b> - Intro to Algorithms & Complexity</ul>
<ul><b>- Computer Science 421A</b> - Foundation of Data Networks</ul>
<ul><b>- Computer Science 423</b> -  Client/Server Programming and Applications</ul>
<ul><b>- Computer Science 431</b> - Introduction to Operating Systems</ul>
<ul><b>- Computer Science 441</b> - Programming Languages: Design and Implementation</ul>
<ul><b>- Computer Science 449</b> - Foundations of Software Engineering</ul>
<ul><b>- Computer Science 451R</b> - Software Engineering Capstone</ul>
<ul><b>- Computer Science 456</b> - Human Computer Interface</ul>
<ul><b>- Computer Science 457</b> - Software Architecture: Requirements & Design</ul>
<ul><b>- Computer Science 461</b> - Introduction to Artificial Intelligence</ul>
<ul><b>- Computer Science 470</b> - Introduction to Database Management Systems</ul>
<ul><b>- Computer Science 490</b> - Special Topics</ul>
<ul><b>- Computer Science 490WD</b> - Special Topics (Web Development)</ul>

<br>

<hr>
