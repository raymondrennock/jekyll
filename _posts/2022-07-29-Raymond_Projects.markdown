---
layout: post
title:  "Projects"
date:   2022-07-29 12:35:13 -0300
categories: UMKC Project
---

<hr>

<br>

This is where I will be publishing links to my projects, as well as my GitHub. However, as of right now this website is also a project that I've worked on. Right now, the main project that I wish to display is this, and as I finish polishing some of my other projects, I will be displaying them here. I will also have images of the programs in action where applicable and a general description. As an example, I will list this own website as a seperate post.

<br> 

<hr> 

<br>

<center>

<h1>GitLab Website Project</h1>

June-July 2022

</center>

<div>
    <p style="float: center;"><img src="https://i.imgur.com/WMLtEla.png" height="400px" width="800px" border="5px"></p>
</div>

For 2 Months, I worked on making this website using Gitlabs, spending time furthering my knowledge of CSS and HTML while learning more about the Jekyll template. As a result, I've gotten to this point where the current website has been made that you are free to click around.

<br>

<hr>
